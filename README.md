# Messages to Matrix

This function is used to get a message from a PubSub topic and send it to a Matrix room. It's used
by my backup system but can be used to send any kind of message in PubSub.

This basic function has the following capacities:

* It extracts the information from a PubSub message.

* If the message is correct, it gets the Matrix configuration from a JSON file located in a Cloud Storage bucket.

* It sends the message to the Matrix room.

## Authentication

This function must have access to Google Cloud (one Cloud Storage bucket)

### Authentication to Google Cloud

This functions uses the default _GOOGLE_APPLICATION_CREDENTIALS_ of the service account who executes it. The service account should have at least the following IAM roles:

* Storage Legacy Bucket Reader

* Storage Object Admin


## Documentation
* [Matrix-nio official documentation](https://matrix-nio.readthedocs.io/en/latest/#api-documentation)

* [Google Cloud Storage documentation for Python 3](https://googleapis.dev/python/storage/latest/client.html)

* [Testing Google Cloud PubSub functions in Python](https://cloud.google.com/functions/docs/testing/test-background#functions-testing-pubsub-unit-python)