import os, sys
import logging
import json

from google.cloud import storage

import apprise

# Vars from Google Cloud environments
GCS_BUCKET = os.getenv("GCS_BUCKET")

def config_logging():
    """This function will configure the logging system
    of the whole function
    """
    
    # Initial configuration of logging system
    logging.basicConfig(
        format="%(asctime)s [%(levelname)s] %(message)s",
        level=logging.INFO,
        stream=sys.stdout
    )


def send_message(message):
    """ This async function will get the configuration to connect to
    Matrix and send a message to a Matrix room.
    """

    # Init logging system
    config_logging()

    # Getting GCS bucket from ENV variables
    GCS_BUCKET = os.getenv("GCS_BUCKET")
    
    # Recovering configuration to connect to Matrix account
    client = storage.Client()
    bucket = client.get_bucket(GCS_BUCKET)
    blob = storage.Blob('config.json', bucket)
    logging.info("Downloading a Matrix credentials from Google Cloud Storage")

    # Checking if the blob already exists
    if blob.exists():
        with open('/tmp/config.json', 'wb') as config_file:
            client.download_blob_to_file(blob, config_file)
    else:
        logging.error("No credentials available. Aborting...")
        exit(1)

    # Loading configuration as JSON config file
    with open('/tmp/config.json') as config_data_file:
        config = json.load(config_data_file)

    # Parsing configuration to create the session into a Matrix server
    server = config['server']
    user = config['user']
    password = config['password']
    room = config['room']

    # Init AppRise instance
    apobj = apprise.Apprise()

    # Define notification channel
    apobj.add(f'matrixs://{user}:{password}@{server}/{room}')
    logging.info("Initializing a Matrix client to send any message")

    # Sending the message to a Matrix room
    apobj.notify(
        body=message,
    )
    logging.info("Sending any message to Matrix room.")


def messages_to_matrix(pubsub_message, context):
    """ This function get the message from the PubSub topic and parse the data
    to send it via Matrix
    """
    # Init logging system
    config_logging()

    # We import base64 library to decode the information
    # inside a PubSub message
    import base64

    # If there is data in the pubsub_message we send it via
    # send_message function
    if 'data' in pubsub_message:
        logging.info("Decrypting PubSub messages to send them using Matrix")
        message = base64.b64decode(pubsub_message['data']).decode('utf-8')
        send_message(message)

        return message
    else:
        return "No data in this message. No message sent"
