import os, sys
from unittest import mock

# We install all  the dependencies in the folder lib
file_path = os.path.dirname(__file__)
module_path = os.path.join(file_path, "../lib")
sys.path.append(module_path)

# We import the main library from the parent folder
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import main

# Create a mock context
mock_context = mock.Mock()
mock_context.event_id = '612345678901234'
mock_context.timestamp = '2023-01-01T22:00:00.000Z'
mock_context.resource = {
    'name': 'projects/my-dummy-project/topics/my-dummy-project',
    'service': 'pubsub.googleapis.com',
    'type': 'type.googleapis.com/google.pubsub.v1.PubsubMessage',
}

# Demo data with "Integration test for messages-to-matrix-function"
mock_context.data = {
    'data': '"SW50ZWdyYXRpb24gdGVzdCBmb3IgbWVzc2FnZXMtdG8tbWF0cml4IGZ1bmN0aW9u"'
}

# Checking e2e functionality in the main checking_backup function
def test_main_function():
    checking = main.messages_to_matrix(mock_context.data, mock_context)
    assert "messages-to-matrix" in checking